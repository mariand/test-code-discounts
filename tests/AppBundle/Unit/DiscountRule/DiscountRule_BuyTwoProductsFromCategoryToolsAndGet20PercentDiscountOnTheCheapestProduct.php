<?php

namespace Tests\AppBundle\Unit\Service;

use AppBundle\DiscountRule\DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProductTest
 * @package Tests\AppBundle\Unit\Service
 */
class DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProductTest extends KernelTestCase
{
    public function testRuleDoesNotApplyForQuantitiesSmallerThan2()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $product1 = new Product();
        $product1
            ->setId(10)
            ->setCategory(1)
            ->setPrice(10)
            ->setDescription('Tool product 1');
        $item1->setProduct($product1)->setUnitPrice(10)->setQuantity(1);
        $order
            ->setCustomerId(1);
        $order
            ->addItem($item1);

        $rule = new DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct($order);
        $rule->applyDiscounts();

        $this->assertEquals(1, count($order->getItems()), 'order has only one item');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'type of item is "normal"');
        $this->assertEquals(1, $order->getItems()[0]->getQuantity(), 'the quantity is not changed');
        $this->assertEquals(10, $order->getTotal(), 'total doesn\'t have a discount applied');
    }

    public function testRuleAppliesForOneProductWithQuantity2()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $product1 = new Product();
        $product1
            ->setId(10)
            ->setCategory(1)
            ->setPrice(10)
            ->setDescription('Tool product 1');
        $item1->setProduct($product1)->setUnitPrice(10)->setQuantity(2);
        $order
            ->setCustomerId(1);
        $order
            ->addItem($item1);

        $rule = new DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct($order);
        $rule->applyDiscounts();

        $this->assertEquals(2, count($order->getItems()), 'order has two items: normal + discount');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'type of item is "normal"');
        $this->assertEquals('discount', $order->getItems()[1]->getType(), 'item 2: discount');
        $this->assertEquals($rule::$ruleDescription, $order->getItems()[1]->getDiscountRule(), 'rule name');
        $this->assertEquals(2, $order->getItems()[0]->getQuantity(), 'the quantity is not changed');
        $this->assertEquals(10 + 0.8 * 10 , $order->getTotal(), 'the total has the discount applied');
    }

    public function testRuleAppliesForTwoProductsAndQuantityGreaterThan2AndDiscountIsAppliedToTheCheapestProduct()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $item2    = new OrderItem();
        $product1 = new Product();
        $product2 = new Product();
        $product1
            ->setId(100)
            ->setCategory(1)
            ->setPrice(100)
            ->setDescription('Tool product 1');
        $product2
            ->setId(10)
            ->setCategory(1)
            ->setPrice(10)
            ->setDescription('Tool product 2');

        $item1->setProduct($product1)->setUnitPrice(100)->setQuantity(2);
        $item2->setProduct($product2)->setUnitPrice(10)->setQuantity(3);
        $order
            ->setCustomerId(1);
        $order
            ->addItem($item1)
            ->addItem($item2);

        $rule = new DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct($order);
        $rule->applyDiscounts();

        $this->assertEquals(3, count($order->getItems()), 'order has three items: 2 * normal + 1 * discount');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'item 1');
        $this->assertEquals('normal', $order->getItems()[1]->getType(), 'item 2');
        $this->assertEquals('discount', $order->getItems()[2]->getType(), 'item 3');
        $this->assertEquals(2, $order->getItems()[0]->getQuantity(), 'quantity of item 1');
        $this->assertEquals(3, $order->getItems()[1]->getQuantity(), 'quantity of item 2');
        $this->assertEquals(1, $order->getItems()[2]->getQuantity(), 'quantity of item 3');
        $this->assertEquals($rule::$ruleDescription, $order->getItems()[2]->getDiscountRule(), 'rule name');
        $this->assertEquals(2 * 100 + 3 * 10 - 0.2 * 10  , $order->getTotal(), 'the total has the discount applied');
    }

    public function testIfProductIsFromCategorySwitches()
    {
        $order   = new Order();
        $rule    = new DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct($order);
        $product = new Product();


        $product->setCategory(1);
        $this->assertTrue($rule->isProductOfCategoryTools($product));

        $product->setCategory(2);
        $this->assertFalse($rule->isProductOfCategoryTools($product));

        $product->setCategory(3);
        $this->assertFalse($rule->isProductOfCategoryTools($product));
    }


}
