<?php

namespace Tests\AppBundle\Unit\Service;

use AppBundle\DiscountRule\DiscountRule_CustomerHasAlreadyBoughtOver1000;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class DiscountRule_CustomerHasAlreadyBoughtOver1000Test
 * @package Tests\AppBundle\Unit\Service
 */
class DiscountRule_CustomerHasAlreadyBoughtOver1000Test extends KernelTestCase
{
    /**
     * @return Customer
     */
    private function getCustomer1000Minus()
    {
        $customer = new Customer();
        $customer
            ->setId(111)
            ->setName("Customer 1000 minus")
            ->setRevenue(123)
            ->setSince('2018-01-01');

        return $customer;
    }

    /**
     * @return Customer
     */
    private function getCustomer1000Plus()
    {
        $customer = new Customer();
        $customer
            ->setId(222)
            ->setName("Customer 1000 plus")
            ->setRevenue(2500)
            ->setSince('2015-01-01');

        return $customer;
    }

    /**
     *
     */
    public function testRuleDoesNotApplyForCustomersThatDoesNotBoughtOver1000()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $product1 = new Product();
        $product1
            ->setId(10)
            ->setCategory(1)
            ->setPrice(100)
            ->setDescription('Product 1');
        $customer = $this->getCustomer1000Minus();
        $item1->setProduct($product1)->setUnitPrice(100)->setQuantity(2);
        $order
            ->setCustomerId($customer->getId())
            ->setCustomer($customer)
            ->addItem($item1);

        $rule = new DiscountRule_CustomerHasAlreadyBoughtOver1000($order);
        $rule->applyDiscounts();


        $this->assertEquals(1, count($order->getItems()), 'order has only one item');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'type of item is "normal"');
        $this->assertEquals(2, $order->getItems()[0]->getQuantity(), 'the quantity is not changed');
        $this->assertEquals(2 * 100, $order->getTotal(), 'the total');
    }

    /**
     *
     */
    public function testRuleApliesForCustomersThatBoughtOver1000()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $product1 = new Product();
        $product1
            ->setId(10)
            ->setCategory(1)
            ->setPrice(100)
            ->setDescription('Product 1');
        $customer = $this->getCustomer1000Plus();
        $item1->setProduct($product1)->setUnitPrice(100)->setQuantity(2);
        $order
            ->setCustomerId($customer->getId())
            ->setCustomer($customer)
            ->addItem($item1);

        $rule = new DiscountRule_CustomerHasAlreadyBoughtOver1000($order);
        $rule->applyDiscounts();

        $this->assertEquals(2, count($order->getItems()), 'order has only one item');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'type of item is "normal"');
        $this->assertEquals('discount', $order->getItems()[1]->getType(), 'type of item is "discount"');
        $this->assertEquals(2, $order->getItems()[0]->getQuantity(), 'the quantity is not changed');
        $this->assertEquals(1, $order->getItems()[1]->getQuantity(), 'the quantity of the discount item is 1');
        $this->assertEquals($rule::$ruleDescription, $order->getItems()[1]->getDiscountRule(), 'rule name');
        $this->assertEquals(2 * 100 - 0.1 * 2 * 100, $order->getTotal(), 'the total');
    }

}
