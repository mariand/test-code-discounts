<?php

namespace Tests\AppBundle\Unit\Service;

use AppBundle\DiscountRule\DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class DiscountRuleBuyFiveProductsFromSwitchesCategoryAndGetOneFreeTest
 * @package Tests\AppBundle\Unit\Service
 */
class DiscountRuleBuyFiveProductsFromSwitchesCategoryAndGetOneFreeTest extends KernelTestCase
{
    public function testRuleDoesNotApplyForQuantitiesSmallerThan5()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $product1 = new Product();
        $product1
            ->setId(100)
            ->setCategory(2)
            ->setPrice(100)
            ->setDescription('Switch product');
        $item1->setProduct($product1)->setUnitPrice(100)->setQuantity(4);
        $order
            ->setCustomerId(1);
        $order
            ->addItem($item1);

        $rule = new DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree($order);
        $rule->applyDiscounts();


        $this->assertEquals(1, count($order->getItems()), 'order has only one item');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'type of item is "normal"');
        $this->assertEquals(4, $order->getItems()[0]->getQuantity(), 'the quantity is not changed ');
        $this->assertEquals(400, $order->getTotal(), 'the total is not changed ');
    }

    public function testRuleAppliesForQuantitiesGreaterThan5()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $product1 = new Product();
        $product1
            ->setId(100)
            ->setCategory(2)
            ->setPrice(100)
            ->setDescription('Switch product');
        $item1->setProduct($product1)->setUnitPrice(100)->setQuantity(7);
        $order
            ->setCustomerId(1);
        $order
            ->addItem($item1);

        $rule = new DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree($order);
        $rule->applyDiscounts();

        $this->assertEquals(2, count($order->getItems()), 'count order items');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'type of the first item');
        $this->assertEquals('discount', $order->getItems()[1]->getType(), 'type of the second item');
        $this->assertEquals(6, $order->getItems()[0]->getQuantity(),
            'the quantity of the normal item was decreased by 1');
        $this->assertEquals(1, $order->getItems()[1]->getQuantity(), 'the quantity of the "discount" item is 1');
        $this->assertEquals(600, $order->getTotal(), 'the total is calculated correct');
    }

    public function testRuleAddOneItemFreeWhenQuantityIs5()
    {
        $order    = new Order();
        $item1    = new OrderItem();
        $product1 = new Product();
        $product1
            ->setId(100)
            ->setCategory(2)
            ->setPrice(100)
            ->setDescription('Switch product');
        $item1->setProduct($product1)->setUnitPrice(100)->setQuantity(5);
        $order
            ->setCustomerId(1);
        $order
            ->addItem($item1);

        $rule = new DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree($order);
        $rule->applyDiscounts();

        $this->assertEquals(2, count($order->getItems()), 'count order items');
        $this->assertEquals('normal', $order->getItems()[0]->getType(), 'type of the first item');
        $this->assertEquals('discount', $order->getItems()[1]->getType(), 'type of the second item');
        $this->assertEquals(5, $order->getItems()[0]->getQuantity(), 'the quantity of the normal item is the same');
        $this->assertEquals(1, $order->getItems()[1]->getQuantity(), 'the quantity of the "discount" item is 1');
        $this->assertEquals(500, $order->getTotal(), 'the total is calculated correct');
    }

    public function testIfProductIsFromCategorySwitches()
    {
        $order   = new Order();
        $rule    = new DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree($order);
        $product = new Product();


        $product->setCategory(2);
        $this->assertTrue($rule->isProductOfCategorySwitches($product));

        $product->setCategory(3);
        $this->assertFalse($rule->isProductOfCategorySwitches($product));


        $product->setCategory(1);
        $this->assertFalse($rule->isProductOfCategorySwitches($product));
    }


}
