<?php

namespace Tests\AppBundle\Unit\Service;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;
use AppBundle\Service\OrderService;
use PHPUnit\Framework\TestCase;

/**
 * Class OrderServiceTest
 * @package Tests\AppBundle\Unit\Service
 */
class OrderServiceTest extends TestCase
{

    private function getTestCustomer()
    {
        $c1 = new Customer();
        $c1->setId(1)->setSince('2011-01-01')->setRevenue(1000)->setName('Customer 1');
    }

    private function getTestOrder()
    {
        $product1 = new Product();
        $product1->setId(1)->setDescription('Product 1')->setPrice(111)->setCategory(1);
        $product2 = new Product();
        $product2->setId(2)->setDescription('Product 2')->setPrice(222)->setCategory(2);

        $c1 = $this->getTestCustomer();

        $order = new Order();
        $order->setId(1)->setTotal(150)->setCustomerId(1)->setCustomer($c1);

        $orderItem1 = new OrderItem();
        $orderItem1->setProductId(1)->setProduct($product1)->setQuantity(1)->setUnitPrice(111)->setTotal(111);
        $orderItem2 = new OrderItem();
        $orderItem2->setProductId(2)->setProduct($product2)->setQuantity(2)->setUnitPrice(222)->setTotal(222);

        $order->addItem($orderItem1);
        $order->addItem($orderItem2);

        return $order;
    }

    public function testGetOrder()
    {
        $testOrder = $this->getTestOrder();

        $orderService = $this->getMockBuilder(OrderService::class)
                                ->disableOriginalConstructor()
                                ->setMethods(['getDeserializedOrder', 'getOrderCustomerById', 'getOrderItemProduct'])
                                ->getMock();

        $orderService
            ->expects($this->any())
            ->method('getDeserializedOrder')
            ->willReturn($testOrder);

        $orderService
            ->expects($this->any())
            ->method('getOrderCustomerById')
            ->willReturn($this->getTestCustomer());


        $product1 = new Product();
        $product1->setId(1)->setDescription('Product 1')->setPrice(111)->setCategory(1);
        $product2 = new Product();
        $product2->setId(2)->setDescription('Product 2')->setPrice(222)->setCategory(2);
        $orderService
            ->expects($this->exactly(2))
            ->method('getOrderItemProduct')
            ->willReturn($product1, $product2);

        $this->assertEquals(2, count($orderService->getOrder(1)->getItems()));

        // todo: make more assertions

    }

}
