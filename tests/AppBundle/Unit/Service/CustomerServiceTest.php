<?php

namespace Tests\AppBundle\Unit\Service;

use AppBundle\Entity\Customer;
use AppBundle\Service\CustomerService;
use PHPUnit\Framework\TestCase;

/**
 * Class CustomerServiceTest
 * @package Tests\AppBundle\Unit\Service
 */
class CustomerServiceTest extends TestCase
{

    private function getTestCustomers()
    {
        $c1 = new Customer();
        $c1->setId(1)->setSince('2011-01-01')->setRevenue(1000)->setName('Customer 1');
        $c2 = new Customer();
        $c2->setId(2)->setSince('2012-02-02')->setRevenue(2000)->setName('Customer 2');
        $c3 = new Customer();
        $c3->setId(3)->setSince('2013-03-03')->setRevenue(3000)->setName('Customer 3');

        return [$c1, $c2, $c3];
    }

    public function testGetCustomerById()
    {
        $customers =$this->getTestCustomers();

        $customerService = $this->getMockBuilder(CustomerService::class)
                                ->disableOriginalConstructor()
                                ->setMethods(['getCustomers'])
                                ->getMock();

        $customerService
            ->expects($this->any())
            ->method('getCustomers')
            ->willReturn($customers);


        $customer1 = $customerService->getCustomerById(1);

        $this->assertEquals(1, $customer1->getId());
        $this->assertEquals('2011-01-01', $customer1->getSince());
        $this->assertEquals(1000, $customer1->getRevenue());
        $this->assertEquals('Customer 1', $customer1->getName());

        $customer2 = $customerService->getCustomerById(2);

        $this->assertEquals(2, $customer2->getId());
        $this->assertEquals('2012-02-02', $customer2->getSince());
        $this->assertEquals(2000, $customer2->getRevenue());
        $this->assertEquals('Customer 2', $customer2->getName());

        $customer3 = $customerService->getCustomerById(3);

        $this->assertEquals(3, $customer3->getId());
        $this->assertEquals('2013-03-03', $customer3->getSince());
        $this->assertEquals(3000, $customer3->getRevenue());
        $this->assertEquals('Customer 3', $customer3->getName());

    }

}
