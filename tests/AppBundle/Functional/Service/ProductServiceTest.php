<?php

namespace Tests\AppBundle\Functional\Service;

use AppBundle\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

class ProductServiceTest extends KernelTestCase
{
    /** @var Container */
    protected $container;

    /** @var ProductService */
    protected $productService;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        parent::bootKernel();

        $this->container = static::$kernel->getContainer();

        $this->productService = $this->container->get('app.product.service');
    }

    public function testGetSerializedProducts()
    {
        $serializedCustomers = $this->productService->getSerializedProducts();
        $expectedJson        = <<<EOT
[
  {
    "id": "A101",
    "description": "Screwdriver",
    "category": "1",
    "price": "9.75"
  },
  {
    "id": "A102",
    "description": "Electric screwdriver",
    "category": "1",
    "price": "49.50"
  },
  {
    "id": "B101",
    "description": "Basic on-off switch",
    "category": "2",
    "price": "4.99"
  },
  {
    "id": "B102",
    "description": "Press button",
    "category": "2",
    "price": "4.99"
  },
  {
    "id": "B103",
    "description": "Switch with motion detector",
    "category": "2",
    "price": "12.95"
  }
]
EOT;

        $this->assertJsonStringEqualsJsonString($expectedJson, $serializedCustomers);
    }

    public function testDeserializeProducts()
    {
        $products = $this->productService->getProducts();

        $this->assertCount(5, $products);

        $this->assertEquals('A101', $products[0]->getId());
        $this->assertEquals('Screwdriver', $products[0]->getDescription());
        $this->assertEquals(1, $products[0]->getCategory());
        $this->assertEquals(9.75, $products[0]->getPrice());

        $this->assertEquals('A102', $products[1]->getId());
        $this->assertEquals('B101', $products[2]->getId());
        $this->assertEquals('B102', $products[3]->getId());
        $this->assertEquals('B103', $products[4]->getId());
    }
}
