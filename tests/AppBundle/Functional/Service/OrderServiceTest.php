<?php

namespace Tests\AppBundle\Functional\Service;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;
use AppBundle\Service\OrderService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class OrderServiceTest
 * @package Tests\AppBundle\Util
 */
class OrderServiceTest extends KernelTestCase
{
    /** @var Container */
    protected $container;

    /** @var OrderService */
    protected $orderService;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        parent::bootKernel();

        $this->container = static::$kernel->getContainer();

        $this->orderService = $this->container->get('app.order.service');
    }

    public function testGetSerializedOrder1()
    {
        $serializedCustomers = $this->orderService->getSerializedOrder(1);
        $expectedJson        = <<<EOT
{
  "id": "1",
  "customer-id": "1",
  "items": [
    {
      "product-id": "B102",
      "quantity": "10",
      "unit-price": "4.99",
      "total": "49.90"
    }
  ],
  "total": "49.90"
}
EOT;

        $this->assertJsonStringEqualsJsonString($expectedJson, $serializedCustomers);
    }

    public function testGetSerializedOrder2()
    {
        $serializedCustomers = $this->orderService->getSerializedOrder(2);
        $expectedJson        = <<<EOT
{
  "id": "2",
  "customer-id": "2",
  "items": [
    {
      "product-id": "B102",
      "quantity": "5",
      "unit-price": "4.99",
      "total": "24.95"
    }
  ],
  "total": "24.95"
}
EOT;

        $this->assertJsonStringEqualsJsonString($expectedJson, $serializedCustomers);
    }

    public function testGetSerializedOrder3()
    {
        $serializedCustomers = $this->orderService->getSerializedOrder(3);
        $expectedJson        = <<<EOT
{
  "id": "3",
  "customer-id": "3",
  "items": [
    {
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    }
  ],
  "total": "69.00"
}

EOT;

        $this->assertJsonStringEqualsJsonString($expectedJson, $serializedCustomers);
    }

    public function testDeserializeOrder1()
    {
        /** @var Order $order */
        $order = $this->orderService->getOrder(1);


        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals(1, $order->getId());
        $this->assertEquals(1, $order->getCustomerId());
        $this->assertEquals(49.90, $order->getTotal());

        //check the customer attached to the order
        $this->assertInstanceOf(Customer::class, $order->getCustomer());
        $this->assertEquals(1, $order->getCustomer()->getId());
        $this->assertEquals('Coca Cola', $order->getCustomer()->getName());
        $this->assertEquals('2014-06-28', $order->getCustomer()->getSince());
        $this->assertEquals(492.12, $order->getCustomer()->getRevenue());

        $this->assertCount(1, $order->getItems());

        //check the order item attached to the order
        $this->assertInstanceOf(OrderItem::class, $order->getItems()[0]);
        $this->assertEquals('normal', $order->getItems()[0]->getType());
        $this->assertEquals('B102', $order->getItems()[0]->getProductId());
        $this->assertEquals(10, $order->getItems()[0]->getQuantity());
        $this->assertEquals(4.99, $order->getItems()[0]->getUnitPrice());
        $this->assertEquals(49.90, $order->getItems()[0]->getTotal());

        // check the product attached to the order
        $this->assertInstanceOf(Product::class, $order->getItems()[0]->getProduct());
        $this->assertEquals('B102', $order->getItems()[0]->getProduct()->getId());
        $this->assertEquals('Press button', $order->getItems()[0]->getProduct()->getDescription());
        $this->assertEquals(2, $order->getItems()[0]->getProduct()->getCategory());
        $this->assertEquals(4.99, $order->getItems()[0]->getProduct()->getPrice());
    }

    public function testDeserializeOrder2()
    {
        /** @var Order $order */
        $order = $this->orderService->getOrder(2);


        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals(2, $order->getId());
        $this->assertEquals(2, $order->getCustomerId());
        $this->assertEquals(24.95, $order->getTotal());

        //check the customer attached to the order
        $this->assertInstanceOf(Customer::class, $order->getCustomer());
        $this->assertEquals(2, $order->getCustomer()->getId());
        $this->assertEquals('Teamleader', $order->getCustomer()->getName());
        $this->assertEquals('2015-01-15', $order->getCustomer()->getSince());
        $this->assertEquals(1505.95, $order->getCustomer()->getRevenue());

        $this->assertCount(1, $order->getItems());

        //check the order item attached to the order
        $this->assertInstanceOf(OrderItem::class, $order->getItems()[0]);
        $this->assertEquals('normal', $order->getItems()[0]->getType());
        $this->assertEquals('B102', $order->getItems()[0]->getProductId());
        $this->assertEquals(5, $order->getItems()[0]->getQuantity());
        $this->assertEquals(4.99, $order->getItems()[0]->getUnitPrice());
        $this->assertEquals(24.95, $order->getItems()[0]->getTotal());

        // check the product attached to the order
        $this->assertInstanceOf(Product::class, $order->getItems()[0]->getProduct());
        $this->assertEquals('B102', $order->getItems()[0]->getProduct()->getId());
        $this->assertEquals('Press button', $order->getItems()[0]->getProduct()->getDescription());
        $this->assertEquals(2, $order->getItems()[0]->getProduct()->getCategory());
        $this->assertEquals(4.99, $order->getItems()[0]->getProduct()->getPrice());
    }

    public function testDeserializeOrder3()
    {
        /** @var Order $order */
        $order = $this->orderService->getOrder(3);


        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals(3, $order->getId());
        $this->assertEquals(3, $order->getCustomerId());
        $this->assertEquals(69.00, $order->getTotal());

        //check the customer attached to the order
        $this->assertInstanceOf(Customer::class, $order->getCustomer());
        $this->assertEquals(3, $order->getCustomer()->getId());
        $this->assertEquals('Jeroen De Wit', $order->getCustomer()->getName());
        $this->assertEquals('2016-02-11', $order->getCustomer()->getSince());
        $this->assertEquals(0, $order->getCustomer()->getRevenue());

        $this->assertCount(2, $order->getItems());

        //check the order item attached to the order
        $this->assertInstanceOf(OrderItem::class, $order->getItems()[0]);
        $this->assertEquals('normal', $order->getItems()[0]->getType());
        $this->assertEquals('A101', $order->getItems()[0]->getProductId());
        $this->assertEquals(2, $order->getItems()[0]->getQuantity());
        $this->assertEquals(9.75, $order->getItems()[0]->getUnitPrice());
        $this->assertEquals(19.50, $order->getItems()[0]->getTotal());

        // check the product attached to the order item
        $this->assertInstanceOf(Product::class, $order->getItems()[0]->getProduct());
        $this->assertEquals('A101', $order->getItems()[0]->getProduct()->getId());
        $this->assertEquals('Screwdriver', $order->getItems()[0]->getProduct()->getDescription());
        $this->assertEquals(1, $order->getItems()[0]->getProduct()->getCategory());
        $this->assertEquals(9.75, $order->getItems()[0]->getProduct()->getPrice());

        // check the product attached to the order item
        $this->assertInstanceOf(OrderItem::class, $order->getItems()[1]);
        $this->assertEquals('normal', $order->getItems()[1]->getType());
        $this->assertEquals('A102', $order->getItems()[1]->getProductId());
        $this->assertEquals(1, $order->getItems()[1]->getQuantity());
        $this->assertEquals(49.50, $order->getItems()[1]->getUnitPrice());
        $this->assertEquals(49.50, $order->getItems()[1]->getTotal());
    }

    public function testSerializeOrder()
    {
        $customer = new Customer();
        $customer
            ->setId('111')
            ->setName('Test Client')
            ->setRevenue(10001)
            ->setSince('2014-01-02');
        $product1 = new Product();
        $product1
            ->setId('C101')
            ->setCategory(3)
            ->setPrice(50)
            ->setDescription('Product one');
        $product2 = new Product();
        $product2
            ->setId('C102')
            ->setCategory(4)
            ->setPrice(20)
            ->setDescription('Product two');

        $orderItem1 = new OrderItem();
        $orderItem1
            ->setType(OrderItem::TYPE_NORMAL)
            ->setQuantity(2)
            ->setProduct($product1)
            ->setProductId($product1->getId())
            ->setUnitPrice($product1->getPrice());

        $orderItem2 = new OrderItem();
        $orderItem2
            ->setType(OrderItem::TYPE_NORMAL)
            ->setQuantity(1)
            ->setProduct($product2)
            ->setProductId($product2->getId())
            ->setUnitPrice($product2->getPrice());

        $order = new Order();
        $order
            ->setId(12)
            ->setCustomerId(111)
            ->setCustomer($customer);

//        $orderItem3 = new OrderItem();
//        $orderItem3
//            ->setType(OrderItem::TYPE_DISCOUNT)
//            ->setDiscountRule('discount rule test')
//            ->setProduct($product1)
//            ->setProductId($product1->getId())
//            ->setQuantity(1)
//            ->setUnitPrice(0);

        $order->addItem($orderItem1);
        $order->addItem($orderItem2);
//        $order->addItem($orderItem3);


        /*
      "product": {
        "id": "C101",
        "description": "Product one",
        "category": "3",
        "price": "50"
      },
      "product": {
        "id": "C102",
        "description": "Product two",
        "category": "4",
        "price": "20"
      },
      "customer": {
        "id": 111,
        "name": "Test Client",
        "since": "2014-01-02",
        "revenue": 10001
      },
         */

        $expectedJson    = <<<EOT
{
  "id": 12,
  "items": [
    {
      "type": "normal",
      "product-id": "C101",
      "quantity": 2,
      "unit-price": 50,
      "total": 100
    },
    {
      "type": "normal",
      "product-id": "C102",
      "quantity": 1,
      "unit-price": 20,
      "total": 20
    }
  ],
  "customer-id": 111,
  "total": 120
}
EOT;
        $serializedOrder = $this->orderService->serializeOrder($order);


        $this->assertJsonStringEqualsJsonString($expectedJson, $serializedOrder);
    }
}
