<?php

namespace Tests\AppBundle\Functional\Service;

use AppBundle\Entity\Customer;
use AppBundle\Service\CustomerService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

class CustomerServiceTest extends KernelTestCase
{
    /** @var Container */
    protected $container;

    /** @var CustomerService */
    protected $customerService;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        parent::bootKernel();

        $this->container = static::$kernel->getContainer();

        $this->customerService = $this->container->get('app.customer.service');
    }

    public function testGetSerializedCustomers()
    {
        $serializedCustomers = $this->customerService->getSerializedCustomers();
        $expectedJson        = <<<EOT
[
  {
    "id": "1",
    "name": "Coca Cola",
    "since": "2014-06-28",
    "revenue": "492.12"
  },
  {
    "id": "2",
    "name": "Teamleader",
    "since": "2015-01-15",
    "revenue": "1505.95"
  },
  {
    "id": "3",
    "name": "Jeroen De Wit",
    "since": "2016-02-11",
    "revenue": "0.00"
  }
]
EOT;

        $this->assertJsonStringEqualsJsonString($expectedJson, $serializedCustomers);
    }

    public function testDeserializeCustomer()
    {
        $customers = $this->customerService->getCustomers();

        $this->assertCount(3, $customers);

        $this->assertInstanceOf(Customer::class, $customers[0]);
        $this->assertEquals(1, $customers[0]->getId());
        $this->assertEquals('Coca Cola', $customers[0]->getName());
        $this->assertEquals('2014-06-28', $customers[0]->getSince());
        $this->assertEquals('492.12', $customers[0]->getRevenue());

        $this->assertInstanceOf(Customer::class, $customers[1]);
        $this->assertEquals(2, $customers[1]->getId());
        $this->assertEquals('Teamleader', $customers[1]->getName());
        $this->assertEquals('2015-01-15', $customers[1]->getSince());
        $this->assertEquals('1505.95', $customers[1]->getRevenue());

        $this->assertInstanceOf(Customer::class, $customers[2]);
        $this->assertEquals(3, $customers[2]->getId());
        $this->assertEquals('Jeroen De Wit', $customers[2]->getName());
        $this->assertEquals('2016-02-11', $customers[2]->getSince());
        $this->assertEquals(0, $customers[2]->getRevenue());
    }

}
