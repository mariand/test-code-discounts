<?php

namespace Tests\AppBundle\Functional\Service;

use AppBundle\DiscountRule\DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree;
use AppBundle\DiscountRule\DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct;
use AppBundle\DiscountRule\DiscountRule_CustomerHasAlreadyBoughtOver1000;
use AppBundle\Entity\OrderItem;
use AppBundle\Service\CustomerService;
use AppBundle\Service\DiscountRuleService;
use AppBundle\Service\OrderService;
use AppBundle\Service\ProductService;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class DiscountRuleServiceTest
 * @package Tests\AppBundle\Service
 */
class DiscountRuleServiceTest extends KernelTestCase
{
    /** @var Container */
    protected $container;

    /** @var CustomerService */
    protected $customerService;

    /** @var ProductService */
    protected $productService;

    /** @var OrderService */
    protected $orderService;

    /** @var DiscountRuleService */
    protected $discountRuleService;

    /** @var Serializer */
    protected $serializer;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        parent::bootKernel();

        $this->container = static::$kernel->getContainer();

        $this->productService      = $this->container->get('app.product.service');
        $this->customerService     = $this->container->get('app.customer.service');
        $this->orderService        = $this->container->get('app.order.service');
        $this->discountRuleService = $this->container->get('app.discountrule.service');

        $this->serializer = $this->container->get('jms_serializer');
    }

    public function test_IfRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree_IsAppliedToOrder_1()
    {
        $order = $this->orderService->getOrder(1);
        $this->discountRuleService
            ->setOrder($order)
            ->applyDiscountsToOrder();


        $this->assertEquals(1, $order->getId(), 'order id = 1');
        $this->assertEquals(44.91, $order->getTotal(), '4.99 * 9 = 44.91');
        $this->assertEquals(1, $order->getCustomerId(), 'customer id');

        $orderItems = $order->getItems();
        $this->assertEquals(2, count($orderItems), 'the order contains two orderItems');

        // We expect order to have two items, one of type normal, with quantity decreased from 10 to 9
        // and another one of type discount, with quantity = 1 and price = 0
        $this->assertEquals('normal', $orderItems[0]->getType());
        $this->assertEquals('B102', $orderItems[0]->getProductId(), 'unchanged');
        $this->assertEquals(4.99, $orderItems[0]->getUnitPrice(), 'unchanged');
        $this->assertEquals(9, $orderItems[0]->getQuantity(),
            'the quantity was decreased by 1 because the sixth product was free because of the promotion');
        $this->assertEquals(44.91, $orderItems[0]->getTotal(),
            'the total price was changed - decreased with the value of a product');

        $this->assertEquals('discount', $orderItems[1]->getType());
        $this->assertEquals('B102', $orderItems[1]->getProductId(), 'unchanged');
        $this->assertEquals(0, $orderItems[1]->getUnitPrice(), 'the product is free so it has zero as price');
        $this->assertEquals(1, $orderItems[1]->getQuantity(), 'one free product');
        $this->assertEquals(0, $orderItems[1]->getTotal(), 'the total is also zero');
        $this->assertEquals(DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree::$ruleDescription,
            $orderItems[1]->getDiscountRule(), 'check the name of the applied rule');
    }

    public function test_RetrieveOnlyDiscountsForOrder1_Rule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree()
    {
        $order = $this->orderService->getOrder(1);
        $this->discountRuleService
            ->setOrder($order)
            ->applyDiscountsToOrder();

        $discounts = $order->getOrderDiscounts();


        $this->assertEquals(1, count($discounts), 'only one discount applied for the order 1');

        $this->assertEquals('discount', $discounts[0]->getType());
        $this->assertEquals('B102', $discounts[0]->getProductId(), 'unchanged');
        $this->assertEquals(0, $discounts[0]->getUnitPrice(), 'the product is free so it has zero as price');
        $this->assertEquals(1, $discounts[0]->getQuantity(), 'one free product');
        $this->assertEquals(0, $discounts[0]->getTotal(), 'the total is also zero');
        $this->assertEquals(
            DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree::$ruleDescription,
            $discounts[0]->getDiscountRule(),
            'check the name of the applied rule');
        // check the original product attached to the OrderItem of type = discount
        $this->assertEquals('B102', $discounts[0]->getProduct()->getId(), '');
        $this->assertEquals('Press button', $discounts[0]->getProduct()->getDescription(), 'product description');
        $this->assertEquals(2, $discounts[0]->getProduct()->getCategory(), 'product category');
        $this->assertEquals(4.99, $discounts[0]->getProduct()->getPrice(), 'original price');
    }


    public function test_CheckOrder2_AfterDiscountRulesAreApplied()
    {
        $order = $this->orderService->getOrder(2);
        $this->discountRuleService
            ->setOrder($order)
            ->applyDiscountsToOrder();

        $this->assertEquals(2, $order->getId(), 'order id = 2');
        $this->assertEquals(2, $order->getCustomerId(), 'customer id');
        $this->assertEquals(22.455, $order->getTotal(), 'discounted total: 24.95 - 2.495 = 22.455');

        $orderItems = $order->getItems();
        $this->assertEquals(3, count($orderItems), 'the order contains one normal orderItems and two discounts applied');

        $this->assertInstanceOf(OrderItem::class, $orderItems[0]);
        $this->assertEquals('normal', $orderItems[0]->getType());
        $this->assertEquals('B102', $orderItems[0]->getProductId(), 'unchanged');
        $this->assertEquals(4.99, $orderItems[0]->getUnitPrice(), 'unchanged');
        $this->assertEquals(5, $orderItems[0]->getQuantity(), 'the quantity will remain unchanged');
        $this->assertEquals(24.95, $orderItems[0]->getTotal(), 'the total will remain unchanged');
        $orderItems[0]->getProduct()->getCategory();

        $this->assertInstanceOf(OrderItem::class, $orderItems[1]);
        $this->assertEquals('discount', $orderItems[1]->getType());
        $this->assertEquals(null, $orderItems[1]->getProductId(), 'the discount is not product related');
        $this->assertEquals(2.495, $orderItems[1]->getUnitPrice(), '10% discount for whole order: 0.1 * 24.95 = 2.495');
        $this->assertEquals(1, $orderItems[1]->getQuantity(), 'one for quantity');
        $this->assertEquals(2.495, $orderItems[1]->getTotal(), 'total discount');
        $this->assertEquals(
            DiscountRule_CustomerHasAlreadyBoughtOver1000::$ruleDescription,
            $orderItems[1]->getDiscountRule(),
            'check the name of the applied rule');

        $this->assertInstanceOf(OrderItem::class, $orderItems[2]);
        $this->assertEquals('discount', $orderItems[2]->getType());
        $this->assertEquals('B102', $orderItems[2]->getProductId(), 'unchanged');
        $this->assertEquals(0, $orderItems[2]->getUnitPrice(), 'zero cost for the free product');
        $this->assertEquals(1, $orderItems[2]->getQuantity(), 'one free');
        $this->assertEquals(0, $orderItems[2]->getTotal(), 'zero cost for the free product');
        $this->assertEquals(
            DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree::$ruleDescription,
            $orderItems[2]->getDiscountRule(),
            'check the name of the applied rule');

        //
        $discountRule = new DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree($order);
        $isProductOfCategorySwitches = $discountRule->isProductOfCategorySwitches($orderItems[2]->getProduct());
        $this->assertTrue($isProductOfCategorySwitches);
    }

    public function test_CheckOrder3_AfterDiscountRulesAreApplied()
    {
        $order = $this->orderService->getOrder(3);
        $this->discountRuleService
            ->setOrder($order)
            ->applyDiscountsToOrder();

        $this->assertEquals(3, $order->getId(), 'order id = 3');
        $this->assertEquals(3, $order->getCustomerId(), 'customer id');
        $this->assertEquals(67.05, $order->getTotal(), 'discounted total: 69.00 - 1.95 = 67.05');

        $orderItems = $order->getItems();
        $this->assertEquals(3, count($orderItems), 'the order contains two original items and one discount ');

        $this->assertInstanceOf(OrderItem::class, $orderItems[0]);
        $this->assertEquals('normal', $orderItems[0]->getType());
        $this->assertEquals('A101', $orderItems[0]->getProductId(), 'unchanged');
        $this->assertEquals(9.75, $orderItems[0]->getUnitPrice(), 'unchanged');
        $this->assertEquals(2, $orderItems[0]->getQuantity(), 'unchanged');
        $this->assertEquals(19.50, $orderItems[0]->getTotal(), 'unchanged');
        $orderItems[0]->getProduct()->getCategory();

        $this->assertInstanceOf(OrderItem::class, $orderItems[1]);
        $this->assertEquals('normal', $orderItems[1]->getType());
        $this->assertEquals('A102', $orderItems[1]->getProductId(), 'unchanged');
        $this->assertEquals(49.50, $orderItems[1]->getUnitPrice(), 'unchanged');
        $this->assertEquals(1, $orderItems[1]->getQuantity(), 'unchanged');
        $this->assertEquals(49.50, $orderItems[1]->getTotal(), 'unchanged');

        $this->assertInstanceOf(OrderItem::class, $orderItems[2]);
        $this->assertEquals('discount', $orderItems[2]->getType());
        $this->assertEquals('A101', $orderItems[2]->getProductId(), 'the same as fist item');
        $this->assertEquals(1.95, $orderItems[2]->getUnitPrice(), '20% discount for the cheapest product (A101): 0.2 * 9.75 = 1.95');
        $this->assertEquals(1, $orderItems[2]->getQuantity(), 'one free');
        $this->assertEquals(1.95, $orderItems[2]->getTotal(), 'total cost');
        $this->assertEquals(
            DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct::$ruleDescription,
            $orderItems[2]->getDiscountRule(),
            'check the name of the applied rule');

        //
        $discountRule = new DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct($order);
        $isProductOfCategoryTools = $discountRule->isProductOfCategoryTools($orderItems[2]->getProduct());
        $this->assertTrue($isProductOfCategoryTools);
    }

}
