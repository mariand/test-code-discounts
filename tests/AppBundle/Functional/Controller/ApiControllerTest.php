<?php

namespace Tests\AppBundle\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApiControllerTest
 * @package Tests\AppBundle\Controller
 */
class ApiControllerTest extends WebTestCase
{
    public function testShowOrder1()
    {
        $client = static::createClient();
        $client->request('GET', '/api/order/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
{
  "id": "1",
  "customer-id": "1",
  "items": [
    {
    "type": "normal",
      "product-id": "B102",
      "quantity": "10",
      "unit-price": "4.99",
      "total": "49.90"
    }
  ],
  "total": "49.90"
}
EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }

    /**
     *
     */
    public function testShowDiscountedOrder1()
    {
        $client = static::createClient();
        $client->request('GET', '/api/order/1/discounted');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
{
  "id": "1",
  "items": [
    {
      "type": "normal",
      "product-id": "B102",
      "quantity": "9",
      "unit-price": "4.99",
      "total": "44.91"
    },
    {
      "type": "discount",
      "discount-rule": "For every product of category \"Switches\" (id 2), when you buy five, you get a sixth for free.",
      "product-id": "B102",
      "quantity": "1",
      "unit-price": "0",
      "total": "0"
    }
  ],
  "customer-id": "1",
  "total": "44.91"
}
EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }

    public function testShowOrder2()
    {
        $client = static::createClient();
        $client->request('GET', '/api/order/2');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
{
  "id": "2",
  "customer-id": "2",
  "items": [
    {
      "type": "normal",
      "product-id": "B102",
      "quantity": "5",
      "unit-price": "4.99",
      "total": "24.95"
    }
  ],
  "total": "24.95"
}

EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }

    /**
     *
     */
    public function testShowDiscountedOrder2()
    {
        $client = static::createClient();
        $client->request('GET', '/api/order/2/discounted');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
{
  "id": "2",
  "items": [
    {
      "type": "normal",
      "product-id": "B102",
      "quantity": "5",
      "unit-price": "4.99",
      "total": "24.95"
    },
    {
      "type": "discount",
      "discount-rule": "A customer who has already bought for over \u20ac 1000, gets a discount of 10% on the whole order.",
      "quantity": "1",
      "unit-price": "2.495",
      "total": "2.495"
    },
    {
      "type": "discount",
      "discount-rule": "For every product of category \"Switches\" (id 2), when you buy five, you get a sixth for free.",
      "product-id": "B102",
      "quantity": "1",
      "unit-price": "0",
      "total": "0"
    }
  ],
  "customer-id": "2",
  "total": "22.455"
}
EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }


    public function testShowOrder3()
    {
        $client = static::createClient();
        $client->request('GET', '/api/order/3');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
{
  "id": "3",
  "customer-id": "3",
  "items": [
    {
      "type": "normal",
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "type": "normal",
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    }
  ],
  "total": "69.00"
}


EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }

    /**
     *
     */
    public function testShowDiscountedOrder3()
    {
        $client = static::createClient();
        $client->request('GET', '/api/order/3/discounted');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
{
  "id": "3",
  "items": [
    {
      "type": "normal",
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "type": "normal",
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    },
    {
      "type": "discount",
      "discount-rule": "If you buy two or more products of category \"Tools\" (id 1), you get a 20% discount on the cheapest product.",
      "product-id": "A101",
      "quantity": "1",
      "unit-price": "1.95",
      "total": "1.95"
    }
  ],
  "customer-id": "3",
  "total": "67.05"
}
EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }


    public function testShowOrder1Discounts()
    {
        $client = static::createClient();
        $client->request('GET', '/api/discounts/order/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
[
  {
    "type": "discount",
    "discount-rule": "For every product of category \"Switches\" (id 2), when you buy five, you get a sixth for free.",
    "product-id": "B102",
    "quantity": "1",
    "unit-price": "0",
    "total": "0"
  }
]
EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }


    public function testShowOrder2Discounts()
    {
        $client = static::createClient();
        $client->request('GET', '/api/discounts/order/2');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
[
  {
    "type": "discount",
    "discount-rule": "A customer who has already bought for over \u20ac 1000, gets a discount of 10% on the whole order.",
    "quantity": "1",
    "unit-price": "2.495",
    "total": "2.495"
  },
  {
    "type": "discount",
    "discount-rule": "For every product of category \"Switches\" (id 2), when you buy five, you get a sixth for free.",
    "product-id": "B102",
    "quantity": "1",
    "unit-price": "0",
    "total": "0"
  }
]
EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }


    public function testShowOrder3Discounts()
    {
        $client = static::createClient();
        $client->request('GET', '/api/discounts/order/3');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"'
        );

        $content = $client->getResponse()->getContent();

        $expectedJson = <<<EOT
[
  {
    "type": "discount",
    "discount-rule": "If you buy two or more products of category \"Tools\" (id 1), you get a 20% discount on the cheapest product.",
    "product-id": "A101",
    "quantity": "1",
    "unit-price": "1.95",
    "total": "1.95"
  }
]
EOT;
        $this->assertJsonStringEqualsJsonString($expectedJson, $content);
    }

}
