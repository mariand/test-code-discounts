<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{
    /**
     * @Route("/api/order/{orderId}", name="api-order")
     *
     * @param Request $request
     * @param string $orderId
     *
     * @return Response
     * @throws \Exception
     */
    public function showOrderAction(Request $request, string $orderId)
    {
        $orderService    = $this->container->get('app.order.service');
        $order           = $orderService->getOrder($orderId);
        $serializedOrder = $orderService->serializeOrder($order);

        $response = new Response($serializedOrder);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/api/order/{orderId}/discounted", name="api-order-discounted")
     *
     * @param Request $request
     * @param string $orderId
     *
     * @return Response
     * @throws \Exception
     */
    public function showDiscountedOrderAction(Request $request, string $orderId)
    {
        $orderService    = $this->container->get('app.order.service');
        $order           = $orderService->getOrder($orderId);
        $discountedOrder = $orderService->getDiscountedOrder();
        $serializedOrder = $orderService->serializeOrder($discountedOrder);

        $response = new Response($serializedOrder);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/api/discounts/order/{orderId}", name="api-discounts-order")
     *
     * @param Request $request
     * @param string $orderId
     *
     * @return Response
     * @throws \Exception
     */
    public function showDiscountsForOrderAction(Request $request, string $orderId)
    {
        $orderService = $this->container->get('app.order.service');

        $order               = $orderService->getOrder($orderId);
        $discountedOrder = $orderService->getDiscountedOrder();
        $serializedDiscounts = $orderService->getSerializedDiscounts();

        $response = new Response($serializedDiscounts);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
