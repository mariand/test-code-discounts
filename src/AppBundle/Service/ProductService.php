<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use JMS\Serializer\SerializerInterface;

/**
 * Class ProductService
 * @package AppBundle\Service
 */
class ProductService {

    /**
     * @var SerializerInterface
     */
	public $serializer;

    /**
     * ProductService constructor.
     *
     * @param SerializerInterface $serializer
     */
	public function __construct( SerializerInterface $serializer ) {
		$this->serializer = $serializer;
	}

    /**
     * @return bool|string
     */
	public function getSerializedProducts() {
		return file_get_contents( __DIR__ . '/../../../app/Resources/data/products.json' );
	}

    /**
     * @return array|\JMS\Serializer\scalar|object|Product[]
     */
	public function getProducts() {
		$serializedProducts = $this->getSerializedProducts();

		return $this->serializer->deserialize( $serializedProducts, 'array<AppBundle\Entity\Product>', 'json' );
	}

    /**
     * @param $id
     *
     * @return Product|mixed|null
     */
    public function getById($id)
    {
        $products = $this->getProducts();

        if ($products) {
            foreach ($products as $product) {
                if ($id == $product->getId()) {
                    return $product;
                }
            }
        }

        return null;
    }
}