<?php

namespace AppBundle\Service;

use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use JMS\Serializer\SerializerInterface;

/**
 * Class OrderService
 * @package AppBundle\Service
 */
class OrderService
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /** @var CustomerService */
    private $customerService;

    /** @var ProductService */
    private $productService;

    /** @var DiscountRuleService */
    private $discountService;

    /**
     * @var Order
     */
    private $order;

    /**
     * OrderService constructor.
     *
     * @param SerializerInterface $serializer
     * @param CustomerService $customerService
     * @param ProductService $productService
     * @param DiscountRuleService $discountService
     */
    public function __construct(
        SerializerInterface $serializer,
        CustomerService $customerService,
        ProductService $productService,
        DiscountRuleService $discountService
    ) {
        $this->serializer      = $serializer;
        $this->customerService = $customerService;
        $this->productService  = $productService;
        $this->discountService  = $discountService;
    }

    /**
     * @param int $orderId
     *
     * @return bool|string
     * @throws \Exception
     */
    public function getSerializedOrder(int $orderId)
    {
        $filePath = __DIR__ . "/../../../app/Resources/example-orders/order{$orderId}.json";

        if (file_exists($filePath)) {
            return file_get_contents($filePath);
        } else {
            throw new \Exception('Order not found');
        }
    }

    /**
     * @param $orderId
     *
     * @return Order|array|\JMS\Serializer\scalar|object
     * @throws \Exception
     */
    public function getDeserializedOrder ($orderId) {

        $serializedOrder = $this->getSerializedOrder($orderId);

        /** @var Order $order */
        $order = $this->serializer->deserialize($serializedOrder, 'AppBundle\Entity\Order', 'json');

        return $order;
    }

    /**
     * @param Order $order
     *
     * @return \AppBundle\Entity\Customer|mixed|null
     */
    public function getOrderCustomerById(Order $order) {
        return $this->customerService->getCustomerById($order->getCustomerId());
    }

    /**
     * @param OrderItem $orderItem
     *
     * @return \AppBundle\Entity\Product|mixed|null
     */
    public function getOrderItemProduct(OrderItem $orderItem) {
        return $this->productService->getById($orderItem->getProductId());
    }

    /**
     * @param int $orderId
     *
     * @return Order|array|\JMS\Serializer\scalar|Order
     * @throws \Exception
     */
    public function getOrder(int $orderId)
    {
        $order = $this->getDeserializedOrder($orderId);


        $this->order = $order;

        // Add customer to order by customer_id
//        $customer = $this->customerService->getCustomerById($order->getCustomerId());
        $customer = $this->getOrderCustomerById($order);
        $order->setCustomer($customer);

        // Add products to OrderItems
        if ($order->getItems()) {
            foreach ($order->getItems() as $item) {
//                $product = $this->productService->getById($item->getProductId());
                $product = $this->getOrderItemProduct($item);
                if ($product) {
                    $item->setProduct($product);
                }
            }
        }

        return $order;
    }

    /**
     * @return Order|null
     */
    public function getDiscountedOrder () {
        if (!$this->order) {
            return null;
        }

        $this->discountService
            ->setOrder($this->order)
            ->applyDiscountsToOrder();

        return $this->order;
    }

    /**
     * @return null|string
     */
    public function getSerializedDiscounts () {
        if (!$this->order) {
            return null;
        }

        return $this->serializer->serialize($this->order->getOrderDiscounts(), 'json');
    }

    /**
     * @param Order $order
     *
     * @return null|string
     */
    public function serializeOrder (Order $order) {
        if (!$order) {
            return null;
        }

        return $this->serializer->serialize($order, 'json');
    }

}