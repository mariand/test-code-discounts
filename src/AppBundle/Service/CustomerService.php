<?php

namespace AppBundle\Service;

use AppBundle\Entity\Customer;
use JMS\Serializer\SerializerInterface;

/**
 * Class CustomerService
 * @package AppBundle\Service
 */
class CustomerService
{
    /**
     * @var SerializerInterface
     */
    public $serializer;

    /**
     * CustomerService constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return bool|string
     */
    public function getSerializedCustomers()
    {
        return file_get_contents(__DIR__ . '/../../../app/Resources/data/customers.json');
    }

    /**
     * @return array|\JMS\Serializer\scalar|object|Customer[]
     */
    public function getCustomers()
    {
        $serializedCustomers = $this->getSerializedCustomers();

        return $this->serializer->deserialize($serializedCustomers, 'array<AppBundle\Entity\Customer>', 'json');
    }

    /**
     * @param $id
     *
     * @return Customer|mixed|null
     */
    public function getCustomerById($id)
    {
        $customers = $this->getCustomers();

        if ($customers) {
            foreach ($customers as $customer) {
                if ($id == $customer->getId()) {
                    return $customer;
                }
            }
        }

        return null;
    }

}