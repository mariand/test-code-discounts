<?php

namespace AppBundle\Service;

use AppBundle\DiscountRule\DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree;
use AppBundle\DiscountRule\DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct;
use AppBundle\DiscountRule\DiscountRule_CustomerHasAlreadyBoughtOver1000;
use AppBundle\DiscountRule\DiscountRuleInterface;
use AppBundle\Entity\Order;
use JMS\Serializer\SerializerInterface;

/**
 * Class DiscountRuleService
 * @package AppBundle\Service
 */
class DiscountRuleService
{
    /** @var Order */
    public $order;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var DiscountRuleInterface[]
     */
    private $availableDiscountRules = [];

    /**
     * DiscountRuleService constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        $this->registerDiscountRules();

        return $this;
    }

    /**
     * TODO: inject rules
     */
    private function registerDiscountRules()
    {

        $this->availableDiscountRules = [
            new DiscountRule_CustomerHasAlreadyBoughtOver1000($this->order),
            new DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree($this->order),
            new DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct($this->order)
        ];

    }

    /**
     * @return DiscountRuleInterface[]
     */
    public function getAvailableRules()
    {
        return $this->availableDiscountRules;
    }

    /**
     * Apply all available discounts to the order.
     * The discounts will be applied in the order that are defined in availableDiscountRules array
     *
     * @todo: check if the many rules can be applied to the order
     */
    public function applyDiscountsToOrder()
    {
        foreach ($this->availableDiscountRules as $discountRule) {
            $discountRule->applyDiscounts();
        }
    }

    /**
     * @return null|string
     */
    public function getSerializedDiscounts()
    {

        if ( ! $this->order) {
            return null;
        }

        return $this->serializer->serialize($this->order->getOrderDiscounts(), 'json');
    }
}