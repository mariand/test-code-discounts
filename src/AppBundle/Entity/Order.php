<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class Order
 */
class Order
{
    /**
     * @var int
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     */
    private $id;

    /**
     * @var OrderItem[]
     *
     * @JMS\Type("ArrayCollection<AppBundle\Entity\OrderItem>")
     */
    private $items;

    /**
     * @var int
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("customer-id")
     */
    private $customerId;

    /**
     * @var Customer
     *
     * @JMS\Exclude()
     */
    public $customer;

    /**
     * @var float
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("total")
     */
    private $total;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Order
     */
    public function setId(int $id): Order
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return OrderItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $items
     *
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @param $item
     *
     * @return $this
     */
    public function addItem($item)
    {
        $this->items->add($item);

        $this->calculateOrder();

        return $this;
    }

    /**
     * Calculate the total based on the order items
     * If item type is discount then the value will be decreased
     */
    public function calculateOrder()
    {
        $total = 0;
        foreach ($this->items as $item) {
            if (OrderItem::TYPE_NORMAL == $item->getType()) {
                $total += $item->getUnitPrice() * $item->getQuantity();
            } else if (OrderItem::TYPE_DISCOUNT == $item->getType()) {
                $total -= $item->getUnitPrice() * $item->getQuantity();
            }
        }

        $this->setTotal($total);
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param $customerId
     *
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param $total
     *
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param $customer
     *
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return OrderItem[]
     */
    public function getOrderDiscounts()
    {
        $discounts = [];

        foreach ($this->items as $item) {
            if (OrderItem::TYPE_DISCOUNT == $item->getType()) {
                $discounts[] = $item;
            }
        }

        return $discounts;
    }


}
