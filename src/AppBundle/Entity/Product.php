<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class Product
 */
class Product
{
	/**
	 * @var string
	 * @JMS\Type("string")
	 * @JMS\SerializedName("id")
	 */
	private $id;

	/**
	 * @var string
	 * @JMS\Type("string")
	 * @JMS\SerializedName("description")
	 */
	private $description;

	/**
	 * @var int
	 * @JMS\Type("string")
	 * @JMS\SerializedName("category")
	 */
	private $category;

	/**
	 * @var float
	 * @JMS\Type("string")
	 * @JMS\SerializedName("price")
	 */
	private $price;

	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id;
	}

	/**
	 * @param string $id
	 *
	 * @return Product
	 */
	public function setId( string $id ): Product {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $description
	 *
	 * @return Product
	 */
	public function setDescription( string $description ): Product {
		$this->description = $description;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getCategory(): int {
		return $this->category;
	}

	/**
	 * @param int $category
	 *
	 * @return Product
	 */
	public function setCategory( int $category ): Product {
		$this->category = $category;

		return $this;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float {
		return $this->price;
	}

	/**
	 * @param float $price
	 *
	 * @return Product
	 */
	public function setPrice( float $price ): Product {
		$this->price = $price;

		return $this;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->description;
	}

}
