<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class Customer
 */
class Customer
{
	/**
	 * @var int
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("id")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("name")
	 */
	private $name;

	/**
     * @var string
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("since")
	 */
	private $since;

	/**
	 * @var float
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("revenue")
	 */
	private $revenue;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 *
	 * @return Customer
	 */
	public function setId( int $id ): Customer {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 *
	 * @return Customer
	 */
	public function setName( string $name ): Customer {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSince() {
		return $this->since;
	}

	/**
	 * @param string $since
	 *
	 * @return Customer
	 */
	public function setSince($since ): Customer {
		$this->since = $since;

		return $this;
	}

	/**
	 * @return float
	 */
	public function getRevenue(): float {
		return $this->revenue;
	}

	/**
	 * @param float $revenue
	 *
	 * @return Customer
	 */
	public function setRevenue( float $revenue ): Customer {
		$this->revenue = $revenue;

		return $this;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->name;
	}

}
