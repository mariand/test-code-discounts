<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class DiscountItem
 * @package AppBundle\Entity
 */
class DiscountItem
{
	/**
	 * @var string
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("name")
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("promo_name")
	 */
	private $promoName;

    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("total")
     */
    private $total;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return DiscountItem
     */
    public function setName(string $name): DiscountItem
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getPromoName(): string
    {
        return $this->promoName;
    }

    /**
     * @param string $promoName
     *
     * @return DiscountItem
     */
    public function setPromoName(string $promoName): DiscountItem
    {
        $this->promoName = $promoName;

        return $this;
    }

    /**
     * @param float $total
     *
     * @return DiscountItem
     */
    public function setTotal(float $total): DiscountItem
    {
        $this->total = $total;

        return $this;
    }

}