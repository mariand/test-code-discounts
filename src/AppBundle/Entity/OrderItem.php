<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class OrderItem
 * @package AppBundle\Entity
 */
class OrderItem
{
    const TYPE_NORMAL = 'normal';
    const TYPE_DISCOUNT = 'discount';

	/**
	 * @var string
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("type")
	 */
	private $type = self::TYPE_NORMAL;

	/**
	 * @var string
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("discount-rule")
	 */
	private $discountRule;

	/**
	 * @var int
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("product-id")
	 */
	private $productId;

	/**
	 * @var Product
     *
     * @JMS\Exclude()
	 */
	private $product;

	/**
	 * @var int
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("quantity")
	 */
	private $quantity;

	/**
	 * @var float
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("unit-price")
	 */
	private $unitPrice;

	/**
	 * @var float
	 *
	 * @JMS\Type("string")
	 * @JMS\SerializedName("total")
	 */
	private $total;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return OrderItem
     */
    public function setType(string $type): OrderItem
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getDiscountRule()
    {
        return $this->discountRule;
    }

    /**
     * @param string $discountRule
     *
     * @return OrderItem
     */
    public function setDiscountRule($discountRule)
    {
        $this->discountRule = $discountRule;

        return $this;
    }

	/**
	 * @return int
	 */
	public function getProductId() {
		return $this->productId;
	}

	/**
	 * @param $productId
	 *
	 * @return $this
	 */
	public function setProductId( $productId ) {
		$this->productId = $productId;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getQuantity() {
		return $this->quantity;
	}

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return OrderItem
     */
    public function setProduct($product): OrderItem
    {
        $this->product = $product;

        return $this;
    }

	/**
	 * @param $quantity
	 *
	 * @return $this
	 */
	public function setQuantity( $quantity ) {
		$this->quantity = $quantity;

        $this->calculateTotal();

		return $this;
	}

	/**
	 * @return float
	 */
	public function getUnitPrice() {
		return $this->unitPrice;
	}

	/**
	 * @param $unitPrice
	 *
	 * @return $this
	 */
	public function setUnitPrice( $unitPrice ) {
		$this->unitPrice = $unitPrice;

		$this->calculateTotal();

		return $this;
	}

    /**
     * Calculate total in case the quantity or unit price has been changed
     */
	private function calculateTotal()
    {
        if ($this->quantity) {
            $this->setTotal($this->unitPrice * $this->quantity);
        }
    }

	/**
	 * @return float
	 */
	public function getTotal() {
		return $this->total;
	}

	/**
	 * @param $total
	 *
	 * @return $this
	 */
	public function setTotal( $total ) {
		$this->total = $total;

		return $this;
	}

}