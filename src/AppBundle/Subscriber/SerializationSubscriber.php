<?php

namespace AppBundle\Subscriber;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Service\CustomerService;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;

/**
 * Class SerializationSubscriber
 * @package AppBundle\Event
 */
class SerializationSubscriber implements EventSubscriberInterface
{
    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * SerializationSubscriber constructor.
     *
     * @param CustomerService $customerService
     */
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event'    => Events::POST_DESERIALIZE,
                'method'   => 'onPostDeserialize',
                'class'    => Order::class,
                'format'   => 'json',
                'priority' => 0
            ],
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onPostDeserialize(ObjectEvent $event)
    {
        /** @var Order $object */
        $object = $event->getObject();
//        var_dump($object);
//        $object->setCustomer('asd');
//        $customer = new Customer();
//        $customer->setId(111)->setName('Name')->setRevenue(123.22);
//        $object->setCustomer($customer);
//
//        $customer = $this->customerService->getCustomerById($object->getCustomerId());
//
//        var_dump('--------');
//        var_dump($customer);

//        if ($object instanceof Order) {
//            $object->setCustomer($customer);
//            var_dump(__LINE__);
//            var_dump($object);
//        }

//

//
//        $visitor = $event->getVisitor();
//        $visitor->addData('customer', $customer);
//
//        var_dump(__FILE__);
//        var_dump($object);

    }
}
