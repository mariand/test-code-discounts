<?php

namespace AppBundle\DiscountRule;

use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;

/**
 * Class DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree
 * @package AppBundle\DiscountRule
 */
class DiscountRule_BuyFiveProductsFromSwitchesCategoryAndGetOneFree extends AbstractDiscountRule
{
    /**
     * @var string
     */
    public static $ruleDescription = 'For every product of category "Switches" (id 2), when you buy five, you get a sixth for free.';

    /**
     *
     */
    public function applyDiscounts()
    {
        if (!$this->order || !$this->order->getItems()) {
            return;
        }

        foreach ($this->order->getItems() as $item) {
            if (
                $item->getProduct() &&
                $this->isProductOfCategorySwitches($item->getProduct())
                && $item->getQuantity() >= 5
            ) {
                // decrease the quantity only if is bigger than 5 (one of items will became free)
                if ($item->getQuantity() > 5) {
                    $item->setQuantity($item->getQuantity() - 1);
                }

                $orderItem = new OrderItem();
                $orderItem
                    ->setType(OrderItem::TYPE_DISCOUNT)
                    ->setDiscountRule($this->getRuleDescription())
                    ->setProduct($item->getProduct())
                    ->setProductId($item->getProduct()->getId())
                    ->setQuantity(1)
                    ->setUnitPrice(0);

                $this->order->addItem($orderItem);
            }
        }
    }

    /**
     * @param Product $product
     *
     * @return bool
     */
    public function isProductOfCategorySwitches(Product $product)
    {
        if (2 == $product->getCategory()) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getRuleDescription()
    {
        return self::$ruleDescription;
    }
}