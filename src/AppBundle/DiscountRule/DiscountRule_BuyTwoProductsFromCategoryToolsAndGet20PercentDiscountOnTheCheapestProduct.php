<?php

namespace AppBundle\DiscountRule;

use AppBundle\Entity\DiscountItem;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Product;

/**
 * Class DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct
 * @package AppBundle\DiscountRule
 */
class DiscountRule_BuyTwoProductsFromCategoryToolsAndGet20PercentDiscountOnTheCheapestProduct extends AbstractDiscountRule
{
    /**
     * @var string
     */
    public static $ruleDescription = 'If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.';

    /**
     *
     */
    public function applyDiscounts()
    {
        /** @var OrderItem|null $cheapestProduct */
        $cheapestProduct = null;
        $quantityOfToolsProducts = 0;

        foreach ($this->order->getItems() as $item) {

            if (
                $item->getProduct() &&
                $this->isProductOfCategoryTools($item->getProduct())
            ) {
                $quantityOfToolsProducts += $item->getQuantity();

                if (
                     null === $cheapestProduct ||
                    $item->getUnitPrice() < $cheapestProduct->getUnitPrice()
                ) {
                    $cheapestProduct = $item;
                }
            }
        }

        if ($cheapestProduct && $quantityOfToolsProducts >= 2) {

            $orderItem = new OrderItem();
            $orderItem
                ->setType(OrderItem::TYPE_DISCOUNT)
                ->setDiscountRule($this->getRuleDescription())
                ->setProduct($cheapestProduct->getProduct())
                ->setProductId($cheapestProduct->getProduct()->getId())
                ->setQuantity(1)
                ->setUnitPrice(0.2 * $cheapestProduct->getUnitPrice())
            ;

            $this->order->addItem($orderItem);
        }
    }

    /**
     * @param Product $product
     *
     * @return bool
     */
    public function isProductOfCategoryTools(Product $product)
    {
        if ($product && 1 == $product->getCategory()) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getRuleDescription()
    {
        return self::$ruleDescription;
    }
}