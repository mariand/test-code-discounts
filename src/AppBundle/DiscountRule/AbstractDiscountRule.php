<?php

namespace AppBundle\DiscountRule;

use AppBundle\Entity\Order;

/**
 * Class AbstractDiscountRule
 * @package AppBundle\DiscountRule
 */
abstract class AbstractDiscountRule implements DiscountRuleInterface
{
    /** @var Order */
    public $order;

    /**
     * @var array
     */
    public $discountItems;

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function __construct(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return \AppBundle\Entity\OrderItem[]
     */
    public function getDiscounts()
    {
        return $this->order->getOrderDiscounts();
    }
}