<?php

namespace AppBundle\DiscountRule;

use AppBundle\Entity\Order;

/**
 * Interface DiscountRuleInterface
 * @package AppBundle\Service
 */
interface DiscountRuleInterface
{
    public function setOrder(Order $order);

    public function applyDiscounts();

    public function getDiscounts();

    public function getRuleDescription();
}