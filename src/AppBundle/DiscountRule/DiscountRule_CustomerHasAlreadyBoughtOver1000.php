<?php

namespace AppBundle\DiscountRule;

use AppBundle\Entity\OrderItem;

/**
 * Class DiscountRule_CustomerHasAlreadyBoughtOver1000
 * @package AppBundle\DiscountRule
 */
class DiscountRule_CustomerHasAlreadyBoughtOver1000 extends AbstractDiscountRule
{
    /**
     * @var string
     */
    public static $ruleDescription = 'A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.';

    /**
     *
     */
    public function applyDiscounts()
    {
        if ($this->areConditionsMet()) {

            $orderItem = new OrderItem();
            $orderItem
                ->setType(OrderItem::TYPE_DISCOUNT)
                ->setDiscountRule($this->getRuleDescription())
                ->setProduct(null)
                ->setProductId(null)
                ->setQuantity(1)
                ->setUnitPrice(0.1 * $this->order->getTotal())
                ->setTotal(0.1 * $this->order->getTotal());

            $this->order->addItem($orderItem);
        }
    }

    /**
     * @return bool
     */
    private function areConditionsMet()
    {
        $customer = $this->order->getCustomer();

        if ($customer->getRevenue() > 1000) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getRuleDescription()
    {
        return self::$ruleDescription;
    }
}