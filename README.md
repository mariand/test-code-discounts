Discounts
==

## Install the application 
`composer install`

## Start the built in web server

You'll need it in order to test the API

`php bin/console server:start --docroot=web  127.0.0.1:8000`

## Running tests

`./vendor/bin/simple-phpunit tests/AppBundle/`

## Assumptions
Many discounts can be applied to an order if the conditions are met.

## The approach
Because a discount rule will affect the original order (total price, number of items) I choose to add the discounts as OrderItems 
(with a specific `type = 'discount'`). 

After applying discounts, an order will contain "normal" items and "discount" items 
that will decrease the total value of the order. 

At this point, discounts are related to the whole order (rule 1) or related to products (rule 2 and 3). In order to be transparent, 
the original items will not be changed, but another "discount" line will decrease the value. 
However, in some cases, the quantity of a normal item can be decreased if the product became free, and another "discount" item will
be added with zero price. 

#### Order 1
The sixth product will became free. The order will contain:
- 9 "normal" items (decreased from 10 to 9)
- 1 "discount" item (the sixth that became free)
#### Order 2
Two discount will be applied:
- 10% discount on the whole order, because the client has already bought for over € 1000
- one free item because already bought 5. In this case a sixth product will be added.
#### Order 3 
 - The normal items will remain unchanged
 - A discount item with 20% of the cheapest product of category Tool will be added

The application expose an API where discounts and orders can be visualized.

##### Original order
`http://127.0.0.1:8000/api/order/{orderId}`

##### Order with discounts applied
`http://127.0.0.1:8000/api/order/{orderId}/discounted`

##### Only discounts of an order
`http://127.0.0.1:8000/api/discounts/order/1`

## Solution explained
For the sake of speed, I choose to use Symfony 3.4 as a framework, mostly because of its dependency injection container.

I used JMS Serializer in order to serialize/deserialize objects from/to json. 
CustomerService, OrderService, ProductService are dealing with (de)serialization process. 
In deserialization process, children objects are created and added to parents. 
For example: adding customer to order, adding product to orderItem.

- Every discount rule has its own class that implements DiscountRuleInterface that defines the contract
- A DiscountRule class has the order as dependency and extends the AbstractPromo class
- In DiscountRuleService the DiscountRule classes should be registered, in order to be used
- At any time, a new DiscountRule_Class can be added with a specific implementation that will not change the behaviour of the others


## Further improvements:
- A mechanism to choose if many rules can be apply to the order or only one
- A mutual exclusion mechanism: some rules cannot be applied with others
- A discount that affect whole order can be applied for the original total or the discounted total
